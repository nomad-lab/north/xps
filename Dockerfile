FROM jupyter/scipy-notebook:2023-04-10

ENV HOME=/home/jovyan
WORKDIR $HOME

ADD requirements.txt .
# Install packages and setup jupyter extensions
RUN pip install -r requirements.txt \
    && jupyter lab build \
    && jupyter nbextension enable --py widgetsnbextension \
    && jupyter serverextension enable jupyterlab_h5web \
RUN rm requirements.txt
